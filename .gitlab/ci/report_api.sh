#!/usr/bin/env bash

if [ ! -f .gitlab/ci/phpcs.phar ]; then
    echo "*** php code sniffer phar does not exists!!"
    exit 1
fi

#php phpcs.phar --standard=mii --report=json . > ./php_code_sniffer_report.json
php .gitlab/ci/phpcs.phar --standard=.gitlab/ci/phpcs.xml root/ --report=json > ./php_code_sniffer_report.json
#php phpmd.phar root/ json cleancode,codesize,controversial,design,naming,unusedcode --report-file ./php_mass_detector.json --exclude vendor/*,.gitlab/* --ignore-errors-on-exit
php .gitlab/ci/phpmd.phar root/ json .gitlab/ci/phpmd.xml --report-file ./php_mass_detector.json --exclude vendor/*,.gitlab/*,tests/*, --ignore-errors-on-exit

if [ ! -f ./php_code_sniffer_report.json ]; then
    echo "*** php code sniffer report does not exists!!"
    exit 1
fi

php .gitlab/ci/code_quality_report_api.php --project_id=$1 --mr_id=$2 --token=$3 --endpoint=$4 --project_dir=$5
