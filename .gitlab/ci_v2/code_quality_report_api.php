<?php

$params = getopt(null, ['project_id:', 'mr_id:', 'token:', 'endpoint:', 'project_dir:']);

$project_id = $params['project_id'];
$mr_id = $params['mr_id'];
$token = $params['token'];
$endpoint = $params['endpoint'];
$project_dir = $params['project_dir'];


define("PROJECT_ID", $project_id);
define("MR_ID", $mr_id);
define("TOKEN", $token);
define("ENDPOINT", $endpoint);


$rawFileReport = file_get_contents(dirname(__FILE__) . '/../../php_code_sniffer_report.json');
$rawFileReportMD = file_get_contents(dirname(__FILE__) . '/../../php_mass_detector.json');

$report = json_decode($rawFileReport, true);
$reportMD = json_decode($rawFileReportMD, true);

if ($report['totals']['errors'] === 0 || $rawFileReport == false && count($reportMD['files']) == 0) {
    post_comment("#### 👏 Code quality check: ![pass](https://raw.githubusercontent.com/SonarSource/sonarcloud-github-static-resources/gh-pages/v2/checks/QualityGateBadge/passed.svg) 👏");

    @unlink(dirname(__FILE__) . '/../../php_code_sniffer_report.json');

} else {

    foreach ($report['files'] as $i => $item) {
        $fileOrigin = str_replace($project_dir. '/','',$i);
        $report['files'][$fileOrigin] = $item;
        unset($report['files'][$i]);
    }

    foreach ($reportMD['files'] as $item) {
        $fileOrigin = str_replace($project_dir. '/','',$item['file']);
        $report['files'][$fileOrigin]['md'] = $item['violations'];
    }

    $result = <<<HEREDOC
#### 😢 Code quality check: ![pass](https://raw.githubusercontent.com/SonarSource/sonarcloud-github-static-resources/gh-pages/v2/checks/QualityGateBadge/failed.svg) 😢
HEREDOC . PHP_EOL;

    foreach ($report['files'] as $file => $messages) {
        $file = preg_replace('/\/builds\/\w+\/\w+\//', '', $file);
        $errors = $messages['errors'];
        $countMD = is_array($messages['md']) ? count($messages['md']) : 0;
        if ($errors == 0 && $countMD == 0) continue;

        $errors += $countMD;

        $result .= PHP_EOL . <<<HEREDOC

👉 **$file**: {- ($errors action need review) -}
HEREDOC . PHP_EOL;
        foreach ($messages['messages'] as $message) {
            $line = $message['line'];
            $msg = $message['message'];
            $result .= <<<HEREDOC
   - _`Line [${line}]`- ${msg}._
HEREDOC . PHP_EOL;
        }

        if ($countMD > 0) {
$result .= PHP_EOL . <<<HEREDOC
----- PHP Mess Detector -----
HEREDOC . PHP_EOL;
            foreach ($messages['md'] as $message) {
                $line = $message['beginLine'];
                $msg = $message['description'];
                $result .= <<<HEREDOC
   - _`Line [${line}]` - ${msg}_
HEREDOC . PHP_EOL;
            }
        }
    }
    post_comment($result);

}


function post_comment($comment) {
    $data = [
        "body" => $comment
    ];
    $data_string = json_encode($data);
    $curl = curl_init(ENDPOINT . PROJECT_ID . "/merge_requests/" . MR_ID . "/notes");

    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'PRIVATE-TOKEN: ' . TOKEN
    ));

    $result = curl_exec($curl);
}
