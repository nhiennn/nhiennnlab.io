<?php

$json = json_decode(file_get_contents("https://crn-api.vgcloud.vn/ajax/api/getdetailprovincial/data.jsx"), true);

$data = [];
foreach ($json['data'] as $city) {
    $data[$city['name']] = [
        'today' => $city['confirmednew'],
        'total' => $city['confirmed'],
        'dead' => $city['deaths'],
        'recovered' => $city['recovered'],
    ];
}

$start = '\033[1;37m';
$end = '\033[0m';

$a36 = '\033[0;36m';
$a35 = '\033[0;35m';
$a34 = '\033[0;34m';
$a33 = '\033[0;33m';
$end = '\033[0m';

$a = <<<EOF
(
    printf "${start}Thành phố${end}\t${start}Ca nhiễm hôm nay${end}\t${start}Tử vong${end}\t${start}Tổng số${end}\n"
EOF;
foreach ($data as $city => $row) {
    $today = $row['today'];
    $total = $row['total'];
    $dead = $row['dead'];
    $format = '';
    if ($today == 0) {
        $format = "${a36}%s${end}\t${a36}%s${end}\t${a36}%s${end}\t${a36}%s${end}\t\n";
    }
    if ($today > 0) {
        $format = "${a34}%s${end}\t${a34}%s${end}\t${a34}%s${end}\t${a34}%s${end}\t\n";
    }
    if ($today > 20) {
        $format = "${a33}%s${end}\t${a33}%s${end}\t${a33}%s${end}\t${a33}%s${end}\t\n";
    }
    if ($today > 100) {
        $format = "${a35}%s${end}\t${a35}%s${end}\t${a35}%s${end}\t${a35}%s${end}\t\n";
    }
    $a .= <<<EOF

printf "$format" "$city" "$today" "$dead" "$total"
EOF;
}
$a .= <<<EOF
) | column -t -s $'\t'
EOF;

echo $a;

